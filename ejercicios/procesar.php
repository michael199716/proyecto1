<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="CSS/estilos.css">
    <link rel="stylesheet" href="index.php">
    <title>Document</title>
</head>
<body id="procesar">
    

</body>
</html>


<?php

$nombre = $_POST['nombre'];
$apellidos = $_POST['apellidos'];

echo '<div id="bloque"><h2>'.$nombre.' '. $apellidos.'</h2></div>';

$provincia = $_POST['provincia'];

$estacion = $_POST['estacion'];
echo "<div id='bloque' require><p>Tu estación preferida para viajar es $estacion</p></div>";

$turismo = $_POST['turismo'];

$puntuacion =$_POST['puntuar'];

$color = $_POST['color'];

if ($color == 'rojo') {
    echo '<body style="background-color:red">';
}
if ($color == 'verde') {
    echo '<body style="background-color:green">';
}
if ($color == 'rosa') {
    echo '<body style="background-color:pink">';
}

if ($provincia == "castellon" && $turismo == "sol y playa") {
    echo "<h2><div id='bloque'>Tu destino va a ser Peñíscola</h2></div>";
    echo "<img id='bloque' src='include/imagenes/peñiscola.jpg' alt=''></div>";
    echo "<p id='bloque'>El mejor momento para viajar a Peñiscola es verano, ya que puedes disfrutar de sus aguas cristalinas. </p>";
    echo "<p id='bloque'>Peñíscola es un lugar idílico donde pasar unos días, donde se mezcla el turismo de sol y playa, familiar y cultural. Si te acercas a este bello enclave descubrirás una ciudad medieval adentrada en el mar, presidida por un orgulloso Castillo Templario en un fantástico estado de conservación rodeado de un hormiguero de calles de trazado medieval que te trasladan por momentos a la misma alpujarra granadina , que a cada paso te sorprenderán con hallazgos tan singulares como su bufador. Peñiscola es sin duda un opción perfecta ya que por lo completo de su oferta turística como por la calidad de la misma no defraudará a nadie, desde el viajero que quiere disfrutar de largas jornadas de sol al que desea descubrir una ciudad medieval.</p>";
}
if ($provincia == "castellon" && $turismo == "montaña") {
    echo "<h2><div id='bloque'>Tu destino va a ser Morella</h2></div>";
    echo "<img id='bloque' src='include/imagenes/morella.jpg' alt=''></div>";
    echo "<p id='bloque'>El mejor momento para viajar a Morella es primavera, ya que las temperaturas son moderadas y es más agradable realizar rutas de senderismo </p>";
    echo "<p id='bloque'>Este fantastico pueblo amurallado medieval esta repleto de rincones con mucho encanto. El parque de aventuras Saltapins esta a pocos minutos en coche de Morella en una pinada donde podras disfrutar de todo tipo de aventuras al aire libre.</p>";
}
if ($provincia == "castellon" && $turismo == "cultural") {
    echo "<h2><div id='bloque'>Tu destino va a ser Mascarell</h2></div>";
    echo "<img id='bloque' src='include/imagenes/mascarell.jpg' alt=''></div>";
    echo "<p id='bloque'>El mejor momento para viajar a Mascarell es primavera, ya que puedes disfrutar de todos sus paisajes con un buen clima. </p>";
    echo "<p id='bloque'>Mascarell es un pueblo único en Castellón, dentro del municipio de Nules. Su singularidad radica en que es el único pueblo de la provincia que está completamente amurallado. Así, una muralla de estilo árabe y de forma rectangular protege todo el pueblo desde hace siglos lo que, rodeado de un entorno agrícola muy singular. Precisamente, esta singularidad le ha valido a Mascarell para ser declarado Bien de Interés Cultural. Esta fortificación fue creada por la población musulmana que fue expulsada de Burriana por Jaime I, a principios del siglo XIV. Tras la expulsión total de la población musulmana, Mascarell quedó prácticamente despoblado hasta el siglo XVIII. Además de poder recrearte de este paisaje único, donde la arquitectura árabe resalta sobre los campos de Nules, destaca su ayuntamiento, del siglo XVIII, y la Iglesia Parroquial del siglo XVII.</p>";
}


if ($provincia == "valencia" && $turismo == "sol y playa") {
    echo "<h2><div id='bloque'>Tu destino va a ser Benidorm</h2></div>";
    echo "<img id='bloque' src='include/imagenes/benidorm.jpg' alt=''></div>";
    echo "<p id='bloque'>El mejor momento para viajar a Benidorm es en verano por las temperaturas</p>";
    echo "<p id='bloque'>Benidorm tiene una gran diversidad de arenales.En primer lugar, la playa de Levante la mas popular esta cuenta con abundantes tumbonas para descansar y restaurantes en el paseo maritimo. Ademas se puede participar en actividades como motos de agua o parapente. En segundo lugar, la playa del Albir situada bastante lejos de Benidorm explica la tranquilidad de esta, y por último la Cala del Mal Pas, es una playa de unos 120 Metros ideal para las parejas que buscan intimidad y tranquiidad</p>";
}
if ($provincia == "valencia" && $turismo == "montaña") {
    echo "<h2><div id='bloque'> Tu destino va a ser Chulilla</h2></div>";
    echo "<img id='bloque' src='include/imagenes/chulilla.jpg' alt=''></div>";
    echo "<p id='bloque'>El mejor momento para viajar a Chulilla es primavera, ya que las temperaturas son moderadas y es más agradable realizar rutas de senderismo </p>";
    echo "<p id='bloque'>Puedes reallizar la ruta de los Calderones o la de los Puentes colgantes; caminar hasta la Peña Judia, zona de bañño del rio Turia; disfrutar del Charco Azul. Realizar la pequeña ruta circular de las Cuevas - Peñeta o tambien se puede subir hasta lo alto del Castillo de Chulilla.</p>";
}
if ($provincia == "valencia" && $turismo == "cultural") {
    echo "<h2><div id='bloque'> Tu destino va a ser Xativa</h2></div><br>";
    echo "<img id='bloque' src='include/imagenes/xativa.jpg' alt=''></div>";
    echo "<p id='bloque'>El mejor momento para viajar a Xativa es en otoño ya que la caida de las hojas le da un color muy bonito</p>";
    echo "<p id='bloque'>Xátiva demuestra a tráves de sus monumentos el lugar privilegiado que ha ocupado en la historia. El Castillo es uno de sus lgares mas visitados por los turistas , la Seu es un edificio del siglo XIX, tambienn posee muchas fuentes com parques y jardines que te recomiendo que no te pierdas ninguna. Tampoco podemos perdernos un paseo por su casco antiguo declarado conjunto historico artistico en 1982. Situado en pleno centro historico, se encuentra el Museo del Almodi.</p>";
}


if ($provincia == "alicante" && $turismo == "sol y playa") {
    echo "<h2><div id='bloque'>Tu destino va a ser Javea</h2></div>";
    echo "<img id='bloque' src='include/imagenes/javea.jpg' alt=''></div>";
    echo "<p id='bloque'>El mejor momento para viajar a Javea es en verano por las temperaturas</p>";
    echo "<p id='bloque'>Merecen un buen baño las playas de Granadella y Cala Ambolo. La primera poque es una cala preciosas de grava y roca que permite la practica del submarinismo por sus aguas cristalinas y la Cala Ambolo por tratarse de una recoleta playa de aguas translucidas de dificil acceso aparte de tener playa por toda su costa.</p>";
}
if ($provincia == "alicante" && $turismo == "montaña") {
    echo "<h2><div id='bloque'> Tu destino va a se Guadalest</h2></div>";
    echo "<img id='bloque' src='include/imagenes/guadalest.jpg' alt=''></div>";
    echo "<p id='bloque'>El mejor momento para viajar a Guadalest es primavera, ya que las temperaturas son moderadas y es más agradable realizar rutas de senderismo </p>";
    echo "<p id='bloque'> Es uno de los pueblos con más encanto del interior de Alicante y uno de los más visitados de la provincia. La localidad es pequeña pero muy cuidadad y con muchos sitios de interés, como los castillos de San José y de la Alcozaiba, la iglesia de la Asunción, la Casa Orduña, la prisión del siglo XII, los museos de Microminiaturas, de Belenes y de las casa de Muñecas </p>";
}
if ($provincia == "alicante" && $turismo == "cultural") {
    echo "<h2><div id='bloque'> Tu destino va a se el Denia</h2></div><br>";
    echo "<img id='bloque' src='include/imagenes/denia.jpg' alt=''></div>";
    echo "<p id='bloque'>El mejor momento para viajar a Denia es en verano ya que tambien tiene playa por toda la costa</p>";
    echo "<p id='bloque'>No debes perderte bajo ningún concepto la subida al castillo y la visita al Museo Arqueologico. Como curiosidad acercate hasta al Museo del Mar y no puedes perderte el Museo del Juguete, es un legado juguetero de la ciudad mientras que el museo Etnologico ubicado en la casa adosada del siglo XIX, contiene obras que narran la vida local. Cerca de allí se encuentra la Iglesia de la Asunción de estilo barroco y el porticado ayuntamiento. </p>";
}

for ($i=0; $i < $puntuacion; $i++) { 
    echo "<img class='puntuacion' src='include/imagenes/llena.png' alt=''></div>";
}
for ($i=5; $i > $puntuacion; $i--) {
    echo "<img class='puntuacion' src='include/imagenes/vacia.png' alt=''></div>";
}



$mensaje = $_POST['mensaje'];
if (isset($mensaje)) {
    echo "<p id='bloque'>$mensaje</p>";
}

$fecha = date('d-m-y h:i:s');
echo "<div id='bloque'><b>$fecha</b></div>";
?>